#include <iostream>
#include <fstream>
#include <iomanip>
#include "lacze_do_gnuplota.hh"
#include <string>
#include <cassert>
#include <unistd.h>
#include <cmath>
#include "Wektor3D.hh"
#include "MacierzRot3D.hh"
#define FPS 60
#define predkosc 1
#define hdrona 80

using namespace std;



PzG::LaczeDoGNUPlota  Lacze;
char c;

class PowierzchniaGeom {
  protected:
   std::string   _NazwaPlik_WspLok;
   Wektor3D      _WekSkali = Wektor3D(1,1,1);
  public:
   PowierzchniaGeom(const std::string &NazwaPliku): _NazwaPlik_WspLok(NazwaPliku) {}
};

  Wektor3D lerp(Wektor3D a, Wektor3D b, double f) 
{
    return (a * (1.0-f)) + b * f;
}


class PowierzchniaGeomScn: public PowierzchniaGeom {
   unsigned int _Ilosc_WieLamanej = 0;
   unsigned int _Ilosc_Lamanych = 0;


   std::string  _NazwaPlik_WspGlb;


  
   Wektor3D     _Trans;  // Definiuje położenie obiektu
   double       _KatX = 0;
   double       _KatY = 0;   // katy do obrotu
   double       _KatZ = 0;

 public:
   PowierzchniaGeomScn(  unsigned int Ilosc_WieLamanej,
		         unsigned int Ilosc_WieLamanych,
		         const std::string & NazwaPlik_WspLok
		       ): PowierzchniaGeom(NazwaPlik_WspLok),
                         _Ilosc_WieLamanej(Ilosc_WieLamanej),
		         _Ilosc_Lamanych(Ilosc_WieLamanych) {}
  
   void ZmienNazwePliku_WspGlb(const std::string & NazwaPlik_WspGlb)
                                 { _NazwaPlik_WspGlb = NazwaPlik_WspGlb; }
   void ZmienPoloznie(const Wektor3D &Pol) { _Trans = Pol; }
   void ZmienKatX (const double &rot)  { _KatX = rot;}
   void ZmienKatY (const double &rot)  { _KatY = rot;}
   void ZmienKatZ (const double &rot)  { _KatZ = rot;}
   double WezWysokosc () {return _Trans[2];}
   bool ObliczWspGlb();
   bool ObliczWspGlbX();
   bool ObliczWspGlbY();

   void RotX(const double &rot);
   void RotY(const double &rot);
   void RotZ(const double &rot);

   void Rusz(const Wektor3D &zmianaPol,const double &Kat);
};






bool PowierzchniaGeomScn::ObliczWspGlb()
{
  assert(!_NazwaPlik_WspLok.empty());
  assert(!_NazwaPlik_WspGlb.empty());
  
  ifstream  StrmWej(_NazwaPlik_WspLok);
  ofstream  StrmWyj(_NazwaPlik_WspGlb);
  Wektor3D  Wsp_pkt;

  MacierzRot3D   MacRot;
  MacierzRot3D   MacSkali;
  for (unsigned int Idx = 0; Idx < 3; ++Idx) 
                             MacSkali(Idx,Idx) = _WekSkali[Idx];
  
  
  
  MacRot.UstawRotZ_st(_KatZ);

  for (unsigned int Ind_Lamanej = 0; Ind_Lamanej < _Ilosc_Lamanych; ++Ind_Lamanej) {
    for (unsigned int Ind_WieLamanej = 0; Ind_WieLamanej < _Ilosc_WieLamanej; ++Ind_WieLamanej) {
      StrmWej >> Wsp_pkt;
      if (StrmWej.fail()) return false;
      Wsp_pkt = MacRot*MacSkali*Wsp_pkt + _Trans;
      StrmWyj << Wsp_pkt << endl;
      if (StrmWyj.fail()) return false;
    }
    StrmWyj << endl;
  }

  
  return true;
}
bool PowierzchniaGeomScn::ObliczWspGlbX()
{
  assert(!_NazwaPlik_WspLok.empty());
  assert(!_NazwaPlik_WspGlb.empty());
  
  ifstream  StrmWej(_NazwaPlik_WspLok);
  ofstream  StrmWyj(_NazwaPlik_WspGlb);
  Wektor3D  Wsp_pkt;

  MacierzRot3D   MacRot;
  MacierzRot3D   MacSkali;
  for (unsigned int Idx = 0; Idx < 3; ++Idx) 
                             MacSkali(Idx,Idx) = _WekSkali[Idx];
  
  
  
  MacRot.UstawRotX_st(_KatX);

  for (unsigned int Ind_Lamanej = 0; Ind_Lamanej < _Ilosc_Lamanych; ++Ind_Lamanej) {
    for (unsigned int Ind_WieLamanej = 0; Ind_WieLamanej < _Ilosc_WieLamanej; ++Ind_WieLamanej) {
      StrmWej >> Wsp_pkt;
      if (StrmWej.fail()) return false;
      Wsp_pkt = MacRot*MacSkali*Wsp_pkt + _Trans;
      StrmWyj << Wsp_pkt << endl;
      if (StrmWyj.fail()) return false;
    }
    StrmWyj << endl;
  }

  
  return true;
}
bool PowierzchniaGeomScn::ObliczWspGlbY()
{
  assert(!_NazwaPlik_WspLok.empty());
  assert(!_NazwaPlik_WspGlb.empty());
  
  ifstream  StrmWej(_NazwaPlik_WspLok);
  ofstream  StrmWyj(_NazwaPlik_WspGlb);
  Wektor3D  Wsp_pkt;

  MacierzRot3D   MacRot;
  MacierzRot3D   MacSkali;
  for (unsigned int Idx = 0; Idx < 3; ++Idx) 
                             MacSkali(Idx,Idx) = _WekSkali[Idx];
  
  
  
  MacRot.UstawRotY_st(_KatY);

  for (unsigned int Ind_Lamanej = 0; Ind_Lamanej < _Ilosc_Lamanych; ++Ind_Lamanej) {
    for (unsigned int Ind_WieLamanej = 0; Ind_WieLamanej < _Ilosc_WieLamanej; ++Ind_WieLamanej) {
      StrmWej >> Wsp_pkt;
      if (StrmWej.fail()) return false;
      Wsp_pkt = MacRot*MacSkali*Wsp_pkt + _Trans;
      StrmWyj << Wsp_pkt << endl;
      if (StrmWyj.fail()) return false;
    }
    StrmWyj << endl;
  }

  
  return true;
}

void PowierzchniaGeomScn::RotX(const double &rot)
{
  Wektor3D Pol;
  MacierzRot3D MatRot;

  MatRot.UstawRotX_st(rot);
  Pol = _Trans;
  for (double x = 0; x<rot; x++)
  {
  (*this).ZmienKatX(x);
  (*this).ObliczWspGlbX();
  Lacze.Rysuj();
  usleep(1000000/FPS); // pozwala na płynne animacje
  
  }

  (*this).ZmienKatX(rot);
  (*this).ObliczWspGlbX();
  Lacze.Rysuj();
  (*this).ZmienPoloznie(MatRot * Pol);
 
}
void PowierzchniaGeomScn::RotY(const double &rot)
{
  Wektor3D Pol;
  MacierzRot3D MatRot;

  MatRot.UstawRotY_st(rot);
  Pol = _Trans;
   for (double x = 0; x<rot; x++)
  {
  (*this).ZmienKatY(x);
  (*this).ObliczWspGlbY();
  Lacze.Rysuj();
  usleep(1000000/FPS); // pozwala na płynne animacje
  }

  (*this).ZmienKatY(rot);
  (*this).ObliczWspGlbY();
  Lacze.Rysuj();
  (*this).ZmienPoloznie(MatRot * Pol);
}
void PowierzchniaGeomScn::RotZ(const double &rot)
{
  Wektor3D Pol;
  MacierzRot3D MatRot;

  MatRot.UstawRotZ_st(rot);
  Pol = _Trans;
   for (double x = 0; x<rot; x++)
  {
  (*this).ZmienKatZ(x);
  (*this).ObliczWspGlb();
  Lacze.Rysuj();
  usleep(1000000/FPS); // pozwala na płynne animacje
  }

  (*this).ZmienKatZ(rot);
  (*this).ObliczWspGlb();
  Lacze.Rysuj();

  (*this).ZmienPoloznie(MatRot * Pol);
}

void PowierzchniaGeomScn::Rusz(const Wektor3D &zmianaPol,const double &Kat)
{
  MacierzRot3D rot;
  Wektor3D nowe, Pol, global;

  rot.UstawRotZ_st(_KatZ);
  global = rot * zmianaPol;
  


  Pol = _Trans + global;
  double odleglosc = sqrt(global[0]*global[0]+global[1]*global[1]+global[2]*global[2]);
  double czas = odleglosc/predkosc;
  int klatki = czas * FPS ;
  
  (*this).RotY(Kat);
  for (int i = 0; i < klatki/30; i++)
  {
    nowe = lerp(_Trans, Pol, (double)i/(klatki));
    if(nowe[2]>115- hdrona/2)
    {
      nowe[2] = 115- hdrona/2; // utrzymanie sie na powierzchni wody
    }
  (*this).ZmienPoloznie(nowe);
  (*this).ObliczWspGlbY();
  Lacze.UstawZakresX(nowe[0]-40, nowe[0]+100);
  Lacze.UstawZakresY(nowe[1]-90, nowe[1]+90);
  Lacze.UstawZakresZ(nowe[2]-10, nowe[2]+120);
  
  Lacze.Rysuj();
  usleep(1000000/FPS); // pozwala na płynne animacje

  
  }
  if (nowe[2] == 115 - hdrona/2)
  {
    Pol[2] = 115- hdrona/2;
  }
  (*this).ZmienPoloznie(Pol);
  (*this).ObliczWspGlb();
  Lacze.Rysuj();
  if(Pol[2] <0)
  {
    cout << "KOLIZJA!!!!!!!! dron machine broke" << endl;
    (*this).ZmienPoloznie(Wektor3D(0,0,10));
  }
  
}


class Prostopadloscian: public PowierzchniaGeomScn {
  public:
   Prostopadloscian(): PowierzchniaGeomScn(4,5,"bryly/prostopadloscian1.dat") {}
};

float WyliczPotencjal(int x, int y)
{
  float  d1 = abs(x-90) + abs(y-55);  // x-90 y-55
  float  d2 = abs(x-30) + abs(y-40);  // x-30 y-40
  float  d  = d1 < d2 ? d1 : d2;
  return d <  15 ? 100 : 100/d;
}
bool ZapiszPolePotDoPliku(const char *NazwaPliku)
{
  string nazwa = NazwaPliku;


  ofstream Strm(NazwaPliku);
  if (Strm.fail()) return false;
  if (nazwa == "bryly/woda.dat")
  {
    for (int x = -600;  x < 600; x += 15) {
    for (int y = -400;  y < 400; y += 15) {
      if(y%10 == 0){
      Strm << x << " " << " " << y << " "
           << 105 << endl;
      }
      else
      {
      Strm << x << " " << " " << y << " "
           << 120 << endl;
      }
      
    }
    Strm << endl;  // Jedna linie zostawiamy wolna
  
  }
  }
  else
  {
  for (int x = -600;  x < 600; x += 10) {
    for (int y = -400;  y < 400; y += 10) {
      Strm << x << " " << " " << y << " "
           << WyliczPotencjal(x,y) << endl;
    }
    Strm << endl;  // Jedna linie zostawiamy wolna
  }
  }
  return true;
}

int main()
{
   Wektor3D przesuniecie;
   MacierzRot3D rot;
  


  Lacze.ZmienTrybRys(PzG::TR_3D);
  Lacze.Inicjalizuj();  // Tutaj startuje gnuplot.

  Lacze.UstawZakresX(-40, 100);
  Lacze.UstawZakresY(-90, 90);
  Lacze.UstawZakresZ(-10, 120);



  Lacze.UstawRotacjeXZ(40,60); // Tutaj ustawiany jest widok
  if (!ZapiszPolePotDoPliku("bryly/model.dat")) {
    cerr << "!!! Nie powiodl sie zapis do pliku: " "bryly/model.dat"
         << endl;
    return 1;
  }
  Lacze.DodajNazwePliku("bryly/model.dat"); // Przekazywana jest nazwa pliku, z
                                        // ktorego maja byc pobrane dane
                                        // do narysowania powierzchni.
  if (!ZapiszPolePotDoPliku("bryly/woda.dat")) {
    cerr << "!!! Nie powiodl sie zapis do pliku: " "bryly/woda.dat"
         << endl;
    return 1;
  }
  Lacze.DodajNazwePliku("bryly/woda.dat");
  Lacze.Rysuj();        // Teraz powinno pojawic sie okienko gnuplota
                        // z rysunkiem.
  Prostopadloscian  Pr;
  Pr.ZmienNazwePliku_WspGlb("bryly/element_drona.dat");
  Lacze.DodajNazwePliku("bryly/element_drona.dat");
  Pr.ZmienPoloznie(Wektor3D(0,0,10));

  Pr.ObliczWspGlb();
  Lacze.Rysuj();        // Teraz powinno pojawic sie okienko gnuplota
                        // z rysunkiem, o ile istnieje plik "prostopadloscian1.dat"
  while (c != 'k')
  { cout << "r - zadaj ruch na wprost" << endl << "o - zadaj zmiane orientacji" << endl << "g - opcja ruchu w pionie i do przodu" << endl << "k - koniec dzialania programu" << endl;
  cout << "Twoj wybor>>";
    cin >> c;
    switch (c){
 
      double Kat, odleglosc;
      case 'o':
      cout << "Podaj kat obrotu drona";
      cout << "Wartosc kata>>"<< endl;
      cin >> Kat;
      Pr.RotZ(Kat);
      break;
      case 'r':
      cout << "Podaj kat nachylenia drona" << endl;
      cout << "Wartosc kata>>"; cin >> Kat;
      cout << "Podaj odleglosc do przebycia przez drona" << endl;
      cout << "Odleglosc>>"; cin >> odleglosc;
      przesuniecie = Wektor3D(odleglosc,0,0);
      rot.UstawRotY_st(-Kat);
      przesuniecie = rot * przesuniecie;
      Pr.Rusz(przesuniecie, -Kat);
      break;
      case 'g':
      double roznica;
      roznica = 115 - Pr.WezWysokosc() - hdrona/2;
      cout << "Podaj odleglosc do podrozy w pionie. Twoja odleglosc od powierzchni to:" << roznica << endl;
      cout << "Odleglosc>>"; cin >> odleglosc;
      przesuniecie = Wektor3D(0,0,odleglosc);
      Pr.Rusz(przesuniecie, 0);
      cout << "Podaj odleglosc do przebycia przez drona" << endl;
      cout << "Odleglosc>>"; cin >> odleglosc;
      przesuniecie = Wektor3D(odleglosc,0,0);
      Pr.Rusz(przesuniecie, 0);
      break;
      case 'k':
      cout << "Koniec Programu" << endl;
      break;
      default:
      cout << "To nie opcja!!!" << endl;

    }

  }
  
}
